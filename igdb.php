<?php
class IGDB {
  function executeRequest($request, $url){
    // set options
    $options = array(
      CURLOPT_URL => $url,
      CURLOPT_HTTPHEADER => array("user-key: " . IGDB_USER_KEY),
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POSTFIELDS => $request
    );

    // perform request
    $cUrl = curl_init();
    curl_setopt_array( $cUrl, $options );
    $response = curl_exec( $cUrl );
    curl_close( $cUrl );

    // decode the response into an array
    $decode = json_decode( $response, true );
    return $decode;
  }

  function searchByName($game_name){
    $filter = "fields *; search \"$game_name\"; exclude tags; limit 1;";
    return $this->executeRequest($filter, 'https://api-v3.igdb.com/games/');
  }

  function getBestGameListByTheme($theme){
      $filter = 'fields name, url, rating, aggregated_rating, first_release_date; where themes.name = "'.$theme.'" & platforms.name = "Xbox One" & rating > 75 & category = 0; exclude tags; limit 10; sort popularity desc;';
      return $this->executeRequest($filter, 'https://api-v3.igdb.com/games/');
  }

  function getTrailerByName($game_name){
    $filter = "fields videos.name, videos.video_id; search \"$game_name\"; exclude tags; limit 1;";
    return $this->executeRequest($filter, 'https://api-v3.igdb.com/games/');
  }

  function getSteamIDByName($game_name){
    $filter = "fields videos.name, videos.video_id; search \"$game_name\"; exclude tags; limit 1;";
    return $this->executeRequest($filter, 'https://api-v3.igdb.com/games/');
  }
}
?>
