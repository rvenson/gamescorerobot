<?php
class SteamAPI {
  function executeRequest($request, $url){
    // set options
    $options = array(
      CURLOPT_URL => $url.$request,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_RETURNTRANSFER => true,
    );

    // perform request
    $cUrl = curl_init();
    curl_setopt_array( $cUrl, $options );
    $response = curl_exec( $cUrl );
    curl_close( $cUrl );

    // decode the response into an array
    $decode = json_decode( $response, true );
    return $decode;
  }

  function getPriceByID(){
    return $this->executeRequest('appdetails?appids=594330', 'https://store.steampowered.com/api/');
  }

}
?>
