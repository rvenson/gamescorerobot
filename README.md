# Game Score Bot for Telegram

A [Telegram Bot](https://core.telegram.org/bots/api) for fetch data from [IGDB](https://api.igdb.com). This is a very early implementation.

## Instalation

To run this bot in production, you will need tree configurations:

1. Create the Telegram Bot at @Bot_Father
2. Create the tokens config file
3. Update the webhook url.

### Create telegram bot
To create a telegram bot on Telegram Bot API, see the [offical documentation](https://core.telegram.org/bots/api).

### Create config file
Create a configuration file in the config folder from the file `.config/auth.php.exemple` and save as `.config/auth.php`

````php
<?php
define('TELEGRAM_BOT_TOKEN', 'YOUR TELEGRAM TOKEN HERE');
define('IGDB_USER_KEY', 'YOUT IGDB TOKEN HERE');
?>
````

### Configuring webhook
This bot works on webhook mode. You need to update the webhook url at telegram bot api. This can be done with the following command:

````bash
curl -F "url=URL" https://api.telegram.org/botTELEGRAM_TOKEN/setWebhook
````
> Replace URL with the full public path to your gamescorerobot.php
> Replace TELEGRAM_TOKEN with your telegram bot token
